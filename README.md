# leetcode-least-jump

The project is to showcase how to write a readable, maintainable and testable 
solution in the author's taste. Not aiming to do it in the fastest way, but 
hoping for great readability, maintainability, confidence and little blunder.

The motivation originated from a discussion with a friend of mine about [this article (Chinese)](https://medium.com/leep3/pythonic-leetcode%E7%B7%B4%E7%BF%92-jump-game-ii-912a5d1294e7).
IMHO that approach is unsystematic and ineffective. One can see the code became 
more and more mysterious after rounds and rounds of refactoring. To prevent my
good friend from learning from what is absolutely wrong, I hereby showcase my 
own, hoping that by seeing what is viable can he understand why not learn from
that article. N.B. No response will be given to strangers.


## Problem Description

[Jump Game II](https://leetcode.com/problems/jump-game-ii/)

Given a random array of positive integers. Player stands at index 0. One can
jump forward by j, 1 <= j <= number of the block. Find the least number of
jumps to the last element. Also, note the key concept to the problem:

Theorem: The best step has the maximum reachable index after jump.


## My Workflow

- Draft 1: Write down the pseudo code, and make a quick sementic skeleton.
- Draft 2: Convert the skeleton into a class.
- Draft 3: Further adjust the structure of the class for readability and convenience of testing.
- Draft 4 (final version): Perform test-driven programming.
    + part 1: write tests for member functions (pytest framework is used)
    + part 2: implement the sub-tasks


## Result

- Time spent: 2.5 hours
- Readability: Great. Should be comprehensive even after years.
- Testability: Great
- Surprises / Places stuck: almost none except the out-of-range error


## Usage

```bash
    # demo
    $ python3 least_jump.py

    # test
    $ pip install pytest
    $ pytest test_least_jump.py
```


## TODO

- Generate Sphinx documentation if I have time...
- Add more anomalies for testing, of course....
