"""
Final Version (draft 4 part 2): implement the remaining blocks in a
    test-driven fashion!

Given a random array of positive integers. Player stands at index 0.
One can jump forward by j, 1 <= j <= number of the block. Find the least
number of jumps to the last element.

Theorem: The best step has the maximum reachable index after jump.

"""


class LeastJump:
    """Process least jump problem"""

    def __init__(self, arr):
        # save constants
        self.arr = arr
        self.idx_max = len(arr) - 1  # game over when reached

    def least_jump(self):
        """Master function: Calculate least jump count in a "jump game"

        :return: jump count
        """
        # pseudo code:
        #
        # initialize
        # while not terminate:
        #     for each jumpable step:
        #          find its maximum reachable index (MRI)
        #          if MRI improves, update MRI and jump size
        #          jump

        # temp variables (only the most top-level is kept)
        idx_now = 0  # current index
        jumps = 0  # jump count

        # main loop
        while not self.chk_terminate(idx_now):
            step = self.compute_step(idx_now)  # (move complex logic into function)
            idx_now += step   # jump is simple
            jumps += 1

        # post-terminaion
        return jumps + 1   # include the last jump

    def chk_terminate(self, idx_now):
        """Termination check: is the last block directly reachable?

        :param idx_now: current player location
        :return: bool
        """
        if idx_now + self.arr[idx_now] >= self.idx_max:
            return True
        else:
            return False

    def compute_step(self, idx_now):
        """Find the step having the Max Reachable Index (MRI) after jump.

        :param idx_now: current player location
        :return: Step size to jump. (Not the MRI!!)
        """
        jump = 0   # jump size corresponding to mri
        mri = 0   # maximum reachable index after jump

        # limit step size to prevent out-of-range error
        if idx_now + self.arr[idx_now] > self.idx_max:
            rng = self.idx_max - idx_now
        else:
            rng = self.arr[idx_now]

        # for each jumpable step
        for step in range(rng, 0, -1):  # rng to 1

            # find its maximum reachable index (MRI)
            tmp_mri = self.find_step_mri(idx_now, step)

            # if MRI improves, update MRI and jump size
            if mri < tmp_mri:
                mri = tmp_mri
                jump = step

        return jump

    def find_step_mri(self, idx_now, step):
        """Find Max Reachable Index after jumping by step

        :param idx_now: current player location
        :param step: step size
        :return: Max Reachable Index
        """
        # now + step + next step
        return idx_now + step + self.arr[idx_now + step]


# Main workflow here!
if __name__ == "__main__":

    # This is done in the test file first and then copied here just to showcase.
    import numpy as np
    
    data = np.array([3,1,4,2,1,3,7], dtype=int)
    lj = LeastJump(data)
    print("Data:")
    print(lj.arr)
    print("Expected jumps = 2 (index = 0 -> 2 -> 6)")
    
    jumps = lj.least_jump()
    print("Computed jumps = {}".format(jumps))
