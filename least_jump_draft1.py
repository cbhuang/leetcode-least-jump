"""
Draft 1: write down pseudo-code and make a sementic skeleton


Given a random array of positive integers. Player stands at index 0.
One can jump forward by j, 1 <= j <= number of the block. Find the least
number of jumps to the last element.

Theorem: The best step has the maximum reachable index after jump.

"""
import numpy as np


def least_jump(arr):
    """Calculate least jump count in a "jump game"

    :param arr (np.array): data
    :return (int): jump count
    """
    # pseudo code:
    #
    # initialize
    # while not terminate:
    #     for each jumpable step:
    #         find its maximum reachable index (MRI)
    #         if MRI improves, update MRI and jump size
    #     jump
    #
    # --------------- The above is my first draft ----------------
    # ----------- which is partially kept in implementation -------------

    # initialize
    idx_max = len(arr) - 1  # game over when reached
    idx_now = 0  # current index
    mri = 0   # maximum reachable index after jump
    jump = 0   # jump size corresponding to mri

    # main loop
    while not terminate(arr, idx_now):
        # for each jumpable step
        for step in range(arr[idx_now], 0, -1):
            # find its maximum reachable index (MRI)
            tmp_mri = find_possible_mri(arr, idx_now)
            # if MRI improves, update MRI and step size
            if mri < tmp_mri:
                mri = tmp_mri
                jump = step
        # jump
