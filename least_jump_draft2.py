"""
Draft 2: Make a class and define sub-tasks

Given a random array of positive integers. Player stands at index 0.
One can jump forward by j, 1 <= j <= number of the block. Find the least
number of jumps to the last element.

Theorem: The best step has the maximum reachable index after jump.

"""

import numpy as np


class LeastJump:
    """Process least jump problem"""

    def __init__(self, arr):
        # save constants
        self.arr = arr
        self.idx_max = len(arr) - 1  # game over when reached

    def least_jump(self):
        """Master function: Calculate least jump count in a "jump game"

        :return (int): jump count
        """
        # pseudo code:
        #
        # initialize
        # while not terminate:
        #     for each jumpable step:
        #         find its maximum reachable index (MRI)
        #         if MRI improves, update MRI and jump size
        #     jump

        # temp variables (not a part of data)
        idx_now = 0  # current index
        mri = 0   # maximum reachable index after jump
        jump = 0   # jump size corresponding to mri

        # main loop
        while not self.chk_terminate(idx_now):
            # for each jumpable step
            for step in range(self.arr[idx_now], 0, -1):
                # find its maximum reachable index (MRI)
                tmp_mri = self.find_possible_mri(idx_now, step)
                # if MRI improves, update MRI and jump size
                if self.mri < tmp_mri:
                    mri = tmp_mri
                    jump = step
            # jump

    def chk_terminate(self, idx_now):
        """Termination check

        :param idx_now: current player location
        :return: bool
        """
        pass

    def find_possible_mri(self, idx_now, step):
        """Find Max Reachable Index after jumping by step

        :param idx_now: current player location
        :param step: step size
        :return: Max Reachable Index
        """
        pass
