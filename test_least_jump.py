"""
Draft 4 part 1: Write some basic tests!

framework pytest
"""
import pytest
import numpy as np
from least_jump import LeastJump


@pytest.fixture
def small_data():
    """regular array"""
    arr = np.array([3,1,4,2,1,3,7], dtype=int)
    # shortest: 3 -> 4 -> 7
    return LeastJump(arr)


def test_check_terminate(small_data):
    data = small_data  # [3,1,4,2,1,3,7]
    assert data.chk_terminate(0) is False
    assert data.chk_terminate(1) is False
    assert data.chk_terminate(2) is True
    assert not data.chk_terminate(3)
    assert not data.chk_terminate(4)
    assert data.chk_terminate(5)
    assert data.chk_terminate(6)


def test_find_step_mri(small_data):
    data = small_data   # [3,1,4,2,1,3,7]
    assert data.find_step_mri(0, 1) == 2
    assert data.find_step_mri(0, 2) == 6
    assert data.find_step_mri(0, 3) == 5
    assert data.find_step_mri(2, 1) == 5
    assert data.find_step_mri(3, 2) == 8


def test_compute_step(small_data):
    data = small_data  # [3,1,4,2,1,3,7]
    assert data.compute_step(0) == 2
    assert data.compute_step(1) == 1
    assert data.compute_step(2) == 4
    assert data.compute_step(3) == 2
    assert data.compute_step(4) == 1
    assert data.compute_step(5) == 1   # mind out of range!


def test_overall(small_data):
    data = small_data  # [3,1,4,2,1,3,7]
    assert data.least_jump() == 2

